// AUTHORS      : TEAM ROCKET
// CREATED ON   : 12-02-2022
// ******************************************************
// This is a demo main file for showing how the Mesh and
// Solver modules that we created can be used  to  solve
// the problem presented in  Project 3  of  the MEEN-689
// Computing Concepts course.
// Kindly refer to the README file for more information.
// ******************************************************

#include<iostream>
#include<iomanip>
// Include the following header files in addition to the
// ones above
#include "SOLVER.h"
#include "Mesh1D.h"
#include<cmath>


// The following rules have been used for nomenclature
// myObj        - for objects
// MyClass      - for classes
// My_Function  - for functions

int main()
{  
    // Define solver input parameters
    double tEnd=20*60;      // Total time (seconds);
    double deltaT=4;        // Time step (seconds)
    double nT=round(tEnd/deltaT);  // Total number of time points
    double T_o=200;          // Initial temperature (kelvin)

    /* Define material objects
    All quantities are specified in SI units
    Arguments: 	    1. Material index
                    2. Material Name
                    3. Material thermal conductivity
                    4. Material density
                    5. Material specific heat capacity
                    6. Material thermal diffusivity
                    7. Material crystallization temperature
    */ 
    Material mat1(1,         "Felt", 1.15, 2230,  830, 6.21e-07,  713.15);
    Material mat2(2, "Carbon_Fibre", 4.00, 1750, 1100, 2.08e-06,  523.15);
    Material mat3(3,        "Epoxy", 0.20, 1100, 1025, 1.77e-07,  493.15);
    Material mat4(4,    "Aluminium",  130, 2810,  933.7, 4.82e-05,  500.00);
    Material mat5(5,        "Steel",   15, 7850,  1505.3, 3.82e-06, 1673.15);

    /* Define layer objects
    Arguments:	1. Layer index
 		        2. Material object associated with this layer
 		        3. Start location
 		        4. Thickness
 		        5. Number of nodes
    */
    // ******** O C T E T  P A I R # 1 ********
    // We will begin by defining the 14 layers of the 1st and 5th octets
    Layer l1(1,   mat1,      0.0,  0.02,  300);
    Layer l2(2,   mat2,  l1.endX, 0.005,  500);
    Layer l3(3,   mat3,  l2.endX, 0.001,  400);
    Layer l4(4,   mat2,  l3.endX, 0.008,  700);
    Layer l5(5,   mat3,  l4.endX, 0.001,  400);
    Layer l6(6,   mat4,  l5.endX, 2.000, 1000);
    Layer l7(7,   mat5,  l6.endX, 1.965, 1000);
    Layer l8(8,   mat5,  l7.endX, 1.965, 1000);
    Layer l9(9,   mat4,  l8.endX, 2.000, 1000);
    Layer l10(10, mat3,  l9.endX, 0.001,  400);
    Layer l11(11, mat2, l10.endX, 0.008,  700);
    Layer l12(12, mat3, l11.endX, 0.001,  400);
    Layer l13(13, mat2, l12.endX, 0.005,  500); 
    Layer l14(14, mat1, l13.endX, 0.02,  300);

    // Next we need to create a set (vector) of the layers we just defined
    std::vector<Layer> layerSet11;
    layerSet11 = {l1, l2,  l3,  l4,  l5,  l6,  l7,
                  l8, l9, l10, l11, l12, l13, l14};
    
    /* We can now create our mesh object
    Arguments: 	    1. Some descriptive identifier for this mesh object
 		            2. Global start node index
    */
    Mesh M1("OctetPair_1_Mesh.txt",0);
    
    /* Using the Mesh_Domain function we will mesh the domain (wow!)
    Arguments:	1. Set (vector) of layers
 		        2. Mesh information Flag (Setting this to true will create a
                                          a mesh information file)
                3. Mesh output filename
    */

    std::vector<Node> myNodes1;
    myNodes1= M1.Mesh_Domain(layerSet11, true, M1.identifier);

    // SOLVER object initialization
    //SOLVER s1(tEnd, nT, to);
    std::cout<<"1st set solution"<<std::endl;

    /* All that is left to be done now is to use the CS_Solver function
       to get the Implicit finite difference solution for the specified
       initial and boundary conditions.
    Arguments:       1. Time step
                     2. Start node heat flux
                     3. End node flux
                     4. Node set
                     5. Job filename
    */

    //declare SOLVER constructor
    SOLVER s1(tEnd,nT,T_o);

    
    // Boundary condition I  (flux only at the start node)
    s1.CS_solver(deltaT, 20000,     0, myNodes1,"set_11.csv");

    // Boundary condition II (flux at start and end node)
    s1.CS_solver(deltaT, 20000, 20000, myNodes1,"set_12.csv");


   // ***********************************************************
   // All the other three pairs can be solved in an exactly
   // similar fashion. We have presented the setup below in 
   // a stepwise manner.
   // ***********************************************************

   // STEP 1 ----- Creating Layer objects -----
   // OCTET PAIR 2 - Octets 2 & 6
    Layer l15(1,  mat1,       0,  0.03,   300);
    Layer l16(2,  mat2, l15.endX, 0.003,  500);
    Layer l17(3,  mat3, l16.endX, 0.001,  400);
    Layer l18(4,  mat2, l17.endX, 0.012,  700);
    Layer l19(5,  mat3, l18.endX, 0.001,  400);
    Layer l20(6,  mat4, l19.endX, 2.300, 1000);
    Layer l21(7,  mat5, l20.endX, 1.653, 1000);
    Layer l22(8,  mat5, l21.endX, 1.653, 1000);
    Layer l23(9,  mat4, l22.endX, 2.300, 1000);
    Layer l24(10, mat3, l23.endX, 0.001,  400);
    Layer l25(11, mat2, l24.endX, 0.012,  700);
    Layer l26(12, mat3, l25.endX, 0.001,  400);
    Layer l27(13, mat2, l26.endX, 0.003,  500);
    Layer l28(14, mat1, l27.endX, 0.03,  300);

    // OCTET PAIR 3 - Octets 3 & 7
    Layer l29(1,  mat1,        0, 0.02,  300);
    Layer l30(2,  mat2, l29.endX, 0.010,  500);
    Layer l31(3,  mat3, l30.endX, 0.001,  400);
    Layer l32(4,  mat2, l31.endX, 0.012,  700);
    Layer l33(5,  mat3, l32.endX, 0.001,  400);
    Layer l34(6,  mat4, l33.endX, 1.700, 1000);
    Layer l35(7,  mat5, l34.endX, 2.256, 1000);
    Layer l36(8,  mat5, l35.endX, 2.256, 1000);
    Layer l37(9,  mat4, l36.endX, 1.700, 1000);
    Layer l38(10, mat3, l37.endX, 0.001,  400);
    Layer l39(11, mat2, l38.endX, 0.012,  700);
    Layer l40(12, mat3, l39.endX, 0.001,  400);
    Layer l41(13, mat2, l40.endX, 0.010,  500);
    Layer l42(14, mat1, l41.endX, 0.02,  300);

    // OCTET PAIR 4 - Octets 4 & 8
    Layer l43(1,  mat1,        0, 0.09,  700);
    Layer l44(2,  mat2, l43.endX, 0.0001,  300);
    Layer l45(3,  mat3, l44.endX, 0.0010,  400);
    Layer l46(4,  mat2, l45.endX, 0.0001,  300);
    Layer l47(5,  mat3, l46.endX, 0.0010,  400);
    Layer l48(6,  mat4, l47.endX, 2.8000, 1000);
    Layer l49(7,  mat5, l48.endX, 1.1928, 1000);
    Layer l50(8,  mat5, l49.endX, 1.1928, 1000);
    Layer l51(9,  mat4, l50.endX, 2.8000, 1000);
    Layer l52(10, mat3, l51.endX, 0.0010,  400);
    Layer l53(11, mat2, l52.endX, 0.0001,  300);
    Layer l54(12, mat3, l53.endX, 0.0010,  400);
    Layer l55(13, mat2, l54.endX, 0.0001,  300);
    Layer l56(14, mat1, l55.endX, 0.09,  700);

    // STEP 2 ----- Creating Layer object sets -----
    std::vector<Layer> layerSet_2={l15,l16,l17,l18,l19,l20,l21,l22,l23,l24,l25,l26,l27,l28};
    std::vector<Layer> layerSet_3={l29,l30,l31,l32,l33,l34,l35,l36,l37,l38,l39,l40,l41,l42};
    std::vector<Layer> layerSet_4={l43,l44,l45,l46,l47,l48,l49,l50,l51,l52,l53,l54,l55,l56};

    // STEP 3 ----- Creating Mesh objects -----
    Mesh M2("OctetPair_2_Mesh.txt",0);
    Mesh M3("OctetPair_3_Mesh.txt",0);
    Mesh M4("OctetPair_4_Mesh.txt",0);

    // STEP 4 ----- Meshing the multilayered domain -----
    std::vector<Node> myNodes2= M2.Mesh_Domain(layerSet_2, true, M2.identifier);
    std::vector<Node> myNodes3= M3.Mesh_Domain(layerSet_3, true, M3.identifier);
    std::vector<Node> myNodes4= M4.Mesh_Domain(layerSet_4, true, M4.identifier);

    // STEP 5 ----- Setting up the solver and solving -----
    std::cout<<"2nd set solution"<<std::endl;
    s1.CS_solver(deltaT, 40000,     0, myNodes2, "set_21.csv");
    s1.CS_solver(deltaT, 40000, 40000, myNodes2, "set_22.csv");

    std::cout<<"3rd set solution"<<std::endl;
    s1.CS_solver(deltaT, 32000,     0, myNodes3, "set_31.csv");
    s1.CS_solver(deltaT, 32000, 32000, myNodes3, "set_32.csv");

    std::cout<<"4th set solution"<<std::endl;
    s1.CS_solver(deltaT, 50000,     0, myNodes4, "set_41.csv");
    s1.CS_solver(deltaT, 50000, 50000, myNodes4, "set_42.csv");

    return 0;
}
#include<fstream>
#include<cctype>
#include"SOLVER.h"
#include <iomanip> // std::left; std::setw
#include<iostream>
#include<armadillo>
#include "Mesh1D.h"
#include<cmath>
#include<cstdlib>
#include<chrono>
#include<stdlib.h>


SOLVER::SOLVER(double t_end, double Nt,double T_o)
        :t_end(t_end), Nt(Nt),T_o(T_o)
        {
            
        }

double SOLVER::writeCSV(arma::mat L,std::vector<Node> myNodes,std::string file_name){

    int M=myNodes.size();
    std::ofstream outdata(file_name);
    for(int i=0;i<M;i++){
        L(0,i)=myNodes[i].xLoc;
    }
    L.save(file_name,arma::csv_ascii);
    return 0;
}

double SOLVER::CS_solver(double delta_t,double q_o1,double q_o2,std::vector<Node> myNodes,std::string file_name){

    // number of nodes
        int N=myNodes.size();
        double F;

    //matrix initialization
        arma::sp_mat K;
        arma::mat save_Mat;
        arma::mat C;
        arma::mat T;
        K.zeros(N,N);
        save_Mat.zeros(Nt+1,N);
        C.zeros(N,1);
        T.ones(N,1);

    //initial condition
        T=T_o*T;

    //time calculation
        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    //Time loop starts
        for(int i=1;i<=Nt;i++){
    //first node
            K(0,0)=-1-2*(myNodes[1].alpha*delta_t/pow(myNodes[1].xStep,2));
            K(0,1)=2*(myNodes[0].alpha*delta_t/pow(myNodes[0].xStep,2));
            C(0,0)=-T(0,0)-((2*delta_t)/(myNodes[0].rho*myNodes[0].cP*myNodes[0].xStep))*q_o1;
            save_Mat(i,0)=T(0);
            //checkTemp(T(0),0,myNodes,i,delta_t);
    //last node
            K(N-1,N-1)=-1-2*(myNodes[N-1].alpha*delta_t/pow(myNodes[N-1].xStep,2));
            K(N-1,N-2)=2*(myNodes[N-2].alpha*delta_t/pow(myNodes[N-2].xStep,2));
            C(N-1,0)=-T(N-1,0)-((2*delta_t)/(myNodes[N-1].rho*myNodes[N-1].cP*myNodes[N-1].xStep))*q_o2;
            save_Mat(i,N-1)=T(N-1);
            //checkTemp(T(N-1),N-1,myNodes,i,delta_t);
    //middle nodes
        for(int j=1;j<=N-2;j++){
            F=(myNodes[j].alpha*delta_t/pow(myNodes[j].xStep,2));
            K(j,j-1)=F/2;
            K(j,j)=-1-F;
            K(j,j+1)=F/2;
            C(j,0)=-T(j,0)-(F/2)*(T(j-1,0)-2*T(j,0)+T(j+1,0));
            save_Mat(i,j)=T(j);
        };

        //sparse matrix solution
        T=arma::spsolve(K,C,"superlu");
    };

    //end of time
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    //time duration calculation
    std::cout << "Time elapsed to solve = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "[ms]" << std::endl;

    //saving mtrix to csv file using function writeCSV
    writeCSV(save_Mat,myNodes,file_name);

    return 0;
}




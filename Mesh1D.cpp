// AUTHOR: TEAM ROCKET
// CONTRIBUTORS: MEGAN H & TANMAYE Y. HEBLEKAR
// DATE & TIME: 11/24/2022 | 7.07 PM
// PROJECT 3 MEEN-689

#include <iostream>
#include <vector>
#include <iomanip>
#include <fstream>
#include "Mesh1D.h"

// The following rules have been used for nomenclature
// myObj        - for objects
// MyClass      - for classes
// My_Function  - for functions

	// Parameterized constructor for Material class
	Material::Material(int para1, std::string para2, float para3, 
		float para4, float para5, float para6, float para7)
	{
		index = para1; 		// Material index
		name = para2; 		// Material name
		k = para3; 			// Material thermal conductivity
		rho = para4; 		// Material density
		cP = para5; 		// Material specific heat capacity
		alpha = para6; 		// Material thermal diffusivity
		tCryst = para7;		// Material crystallization temperature
	}

	// Parameterized constructor for Layer class
	Layer::Layer(int para1, Material para2, float para3, 
	      		 float para5, int para6)
	      : material(para2) // Member Initializer List for passing 
	                        // object as constructor parameter
	{
		index = para1; 			// Layer index
		startX = para3; 		// Layer start coordinate
		thickness = para5; 		// Layer thickness
		nodeCount = para6; 		// Layer node count
		endX = para3 + para5; 	// Layer end coordinate (calculated)
	}
	
	// Parameterized constructor for MeshLayer class
	MeshLayer::MeshLayer(Layer para1, int para2)
	      	  : layer(para1) // Member Initializer List for passing 
	                     	 // object as constructor parameter
	{
		// Layer has been passed via list assignment
		startNode = para2; // Start node label of the layer
		endNode = startNode+nodeCount-1; // End node label
	}
	
	// Function to generate nodes inside the associated layer
	// Layer to be meshed is already assigned in the constructor
	std::vector<Node> MeshLayer::Mesh_Layer()
	{
		Node ni; // Declare an instance of type 'Node'
		
		Material nodeMat = layer.material; // Retrieve material of the layer
										   // to be meshed
		
		int i = startNode; // 'i' serves as a counter for the node labels
		
		float startX = layer.startX; // Retrieve the start coordinate of the
									 // layer to be meshed
		
		float delx = layer.thickness/(nodeCount-1); // Spacing between nodes
		
		std::vector<Node> nodeSet; // Vector of nodes. This set shall contain
								   // all nodes associated with a single
								   // specified layer
		
		for(int j=0;j<nodeCount;j++)
		{
			// Populate attributes for the ith node
			ni.index = i; 				// Node index AKA label
			ni.xLoc = startX+j*delx;	// Node location
			ni.xStep = delx;			// Associated step size
			ni.matName = nodeMat.name;	// Node material name
			// Following are material properties associated
			// to the Node object
			ni.k = nodeMat.k;
			ni.rho = nodeMat.rho;
			ni.cP = nodeMat.cP;
			ni.alpha = nodeMat.alpha;
			ni.tCryst = nodeMat.tCryst;
			// Push back node into the set
			nodeSet.push_back(ni);
			i++;
		}
		return nodeSet;
	}

	// Parameterized constructor for Mesh class
	Mesh::Mesh(std::string para1, int para2)
	{
		identifier = para1; // String identifier e.g. "Mesh for octet 1"
		startNode = para2;  // Global start node index
	}
	
	// Function to create set of all nodes in the domain
	std::vector<Node> Mesh::Mesh_Domain(std::vector<Layer> layerSet, 
										bool meshInfoFlag, 
										std::string meshFilename)
	{
	/*	Arguments:	1. layerSet 	- Set of layers that make up the domain
					2. meshInfoFlag	- Flag (True when data needs to be written)
	*/
					
		std::vector<Node> nodeSet; 	// This set stores all the nodes in the domain
		int k = startNode;			// Rename this variable for better readability
		
		for (int i=0;i<layerSet.size();i++)
		{	
			MeshLayer tempML(layerSet[i],k); // Create a MeshLayer object
			std::vector<Node> tempNodeSet;	 // Create a temp node set
			
			// Generate nodes within the ith layer using the following
			// function
			tempNodeSet = tempML.Mesh_Layer();

			// delete extra interface node from the preceding layer
			// exclude this operation for the last layer (for obvious reasons)
			if(i<layerSet.size()-1)
			{
				tempNodeSet.erase(tempNodeSet.end()-1);
			}
			
			// Push all newly created nodes into nodeSet from the back
			for(int j=0;j<tempNodeSet.size();j++)
			{
				nodeSet.push_back(tempNodeSet[j]);
				k++; // Increment the node label counter
			}
		}
		// Write mesh information to a text file if the user desires so
		if(meshInfoFlag)
		{
			// Declare a ofstream object
			std::ofstream myFile(meshFilename);
			myFile<<"# MESH INFORMATION:"<<std::endl;
			myFile<<"# "
				  <<layerSet.size()
				  <<" layers are present in the model"
				  <<std::endl;
			myFile<<"# "
				  <<nodeSet.size()
				  <<" nodes have been created"
				  <<std::endl;
			
			int colSize=15;
			// Write the header row
			myFile
			<<std::right<<std::setw(colSize)<<"NODE ID"
			<<std::right<<std::setw(colSize)<<"X COORD"
			<<std::right<<std::setw(colSize)<<"DELTA X"
			<<std::right<<std::setw(colSize)<<"MATERIAL"
			<<std::right<<std::setw(colSize)<<"K"
			<<std::right<<std::setw(colSize)<<"RHO"
			<<std::right<<std::setw(colSize)<<"CP"
			<<std::right<<std::setw(colSize)<<"ALPHA"
			<<std::right<<std::setw(colSize)<<"CRYST TEMP"
			<<std::endl;

			for(int i=0;i<nodeSet.size();i++)
			{
				// Write the mesh data
				myFile
				<<std::right<<std::setw(colSize)<<nodeSet[i].index
				<<std::right<<std::setw(colSize)<<nodeSet[i].xLoc
				<<std::scientific<<std::setprecision(4)
				<<std::right<<std::setw(colSize)<<nodeSet[i].xStep
				<<std::right<<std::setw(colSize)<<nodeSet[i].matName
				<<std::right<<std::setw(colSize)<<nodeSet[i].k
				<<std::right<<std::setw(colSize)<<nodeSet[i].rho
				<<std::right<<std::setw(colSize)<<nodeSet[i].cP
				<<std::right<<std::setw(colSize)<<nodeSet[i].alpha
				<<std::right<<std::setw(colSize)<<nodeSet[i].tCryst
				<<std::endl;		
			}
		}
		return nodeSet;
	}
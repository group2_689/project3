import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
import numpy as np
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits import mplot3d


#input timestep used
delta_t=4;  #in seconds

#reading mesh files and obtaining cordinates,material names and melting temperature limits
m1=open('OctetPair_1_Mesh.txt','r')
m2=open('OctetPair_2_Mesh.txt','r')
m3=open('OctetPair_3_Mesh.txt','r')
m4=open('OctetPair_4_Mesh.txt','r')

x1=m1.readlines()
x2=m2.readlines()
x3=m3.readlines()
x4=m4.readlines()

finallist1 = []
finallist2 = []
finallist3 = []
finallist4 = []

for i in x1[4:]:
    temp=i.split(" ")
    temp1=[i for i in temp if i]
    finallist1.append(temp1)

for i in x2[4:]:
    temp=i.split(" ")
    temp1=[i for i in temp if i]
    finallist2.append(temp1)

for i in x3[4:]:
    temp=i.split(" ")
    temp1=[i for i in temp if i]
    finallist3.append(temp1)

for i in x4[4:]:
    temp=i.split(" ")
    temp1=[i for i in temp if i]
    finallist4.append(temp1)

cord1=[]
cord2=[]
cord3=[]
cord4=[]

mat1=[]
mat2=[]
mat3=[]
mat4=[]

t1=[]
t2=[]
t3=[]
t4=[]


for i in range (0,len(finallist1)-1):
    cord1.append(float(finallist1[i][1]))
    t1.append(float(finallist1[i][8]))
    mat1.append(finallist1[i][3])

for i in range (0,len(finallist2)-1):
    cord2.append(float(finallist2[i][1]))
    t2.append(float(finallist2[i][8]))
    mat2.append(finallist2[i][3])

for i in range (0,len(finallist3)-1):
    cord3.append(float(finallist3[i][1]))
    t3.append(float(finallist3[i][8]))
    mat3.append(finallist3[i][3])

for i in range (0,len(finallist4)-1):
    cord4.append(float(finallist4[i][1]))
    t4.append(float(finallist4[i][8]))
    mat4.append(finallist4[i][3])



#reading result output file

df11 = pd.read_csv('set_11.csv',header=None)
ar11=df11.to_numpy()
df12 = pd.read_csv('set_12.csv',header=None)
ar12=df12.to_numpy()
df21 = pd.read_csv('set_21.csv',header=None)
ar21=df21.to_numpy()
df22 = pd.read_csv('set_22.csv',header=None)
ar22=df22.to_numpy()
df31 = pd.read_csv('set_31.csv',header=None)
ar31=df31.to_numpy()
df32 = pd.read_csv('set_32.csv',header=None)
ar32=df32.to_numpy()
df41 = pd.read_csv('set_41.csv',header=None)
ar41=df41.to_numpy()
df42 = pd.read_csv('set_42.csv',header=None)
ar42=df42.to_numpy()


#time array
tt=[]
sum=0
for i in range(0,len(ar11)):
    tt.append(sum)
    sum=sum+i

#plotting
print('Now plotting the results')

fig11 = plt.figure()
ax11 = fig11.add_subplot(111)
X,Y=np.mgrid[0:ar11.shape[1],0:len(ar11)]
surface11 = ax11.contourf(X,Y*delta_t/60,ar11.transpose(),cmap='jet')
ax11.set_title('Set 1:- BC1 solution')
ax11.set_xlabel('Nodes (along the length)')
ax11.set_ylabel('Time (min)')
plt.colorbar(surface11,label='Temperature (K)')
plt.savefig('set_11_plot.png',dpi=400)
#plt.show()

fig12 = plt.figure()
ax12 = fig12.add_subplot(111)
X,Y=np.mgrid[0:ar12.shape[1],0:len(ar12)]
surface12 = ax12.contourf(X,(Y*delta_t)/60,ar12.transpose(),cmap='jet')
ax12.set_title('Set 1:- BC2 solution')
ax12.set_xlabel('Nodes (along the length)')
ax12.set_ylabel('Time (min)')
plt.colorbar(surface12,label='Temperature (K)')
plt.savefig('set_12_plot.png',dpi=400)
#plt.show()


fig21 = plt.figure()
ax21 = fig21.add_subplot(111)
X,Y=np.mgrid[0:ar21.shape[1],0:len(ar21)]
surface21 = ax21.contourf(X,Y*delta_t/60,ar21.transpose(),cmap='jet')
ax21.set_title('Set 2:- BC1 solution')
ax21.set_xlabel('Nodes (along the length)')
ax21.set_ylabel('Time (min)')
plt.colorbar(surface21,label='Temperature (K)')
plt.savefig('set_21_plot.png',dpi=400)
#plt.show()

fig22 = plt.figure()
ax22 = fig22.add_subplot(111)
X,Y=np.mgrid[0:ar22.shape[1],0:len(ar22)]
surface22 = ax22.contourf(X,Y*delta_t/60,ar22.transpose(),cmap='jet')
ax22.set_title('Set 2:- BC2 solution')
ax22.set_xlabel('Nodes (along the length)')
ax22.set_ylabel('Time (min)')
plt.colorbar(surface22,label='Temperature (K)')
plt.savefig('set_22_plot.png',dpi=400)
#plt.show()

fig31 = plt.figure()
ax31 = fig31.add_subplot(111)
X,Y=np.mgrid[0:ar31.shape[1],0:len(ar31)]
surface31 = ax31.contourf(X,Y*delta_t/60,ar31.transpose(),cmap='jet')
ax31.set_title('Set 3:- BC1 solution')
ax31.set_xlabel('Nodes (along the length)')
ax31.set_ylabel('Time (min)')
plt.colorbar(surface31,label='Temperature (K)')
plt.savefig('set_31_plot.png',dpi=400)
#plt.show()

fig32 = plt.figure()
ax32 = fig32.add_subplot(111)
X,Y=np.mgrid[0:ar32.shape[1],0:len(ar32)]
surface32 = ax32.contourf(X,Y*delta_t/60,ar32.transpose(),cmap='jet')
ax32.set_title('Set 3:- BC2 solution')
ax32.set_xlabel('Nodes (along the length)')
ax32.set_ylabel('Time (min)')
plt.colorbar(surface32,label='Temperature (K)')
plt.savefig('set_32_plot.png',dpi=400)
#plt.show()

fig41 = plt.figure()
ax41 = fig41.add_subplot(111)
X,Y=np.mgrid[0:ar41.shape[1],0:len(ar41)]
surface41 = ax41.contourf(X,Y*delta_t/60,ar41.transpose(),cmap='jet')
ax41.set_title('Set 4:- BC1 solution')
ax41.set_xlabel('Nodes (along the length)')
ax41.set_ylabel('Time (min)')
plt.colorbar(surface41,label='Temperature (K)')
plt.savefig('set_41_plot.png',dpi=400)
#plt.show()

fig42 = plt.figure()
ax42 = fig42.add_subplot(111)
X,Y=np.mgrid[0:ar42.shape[1],0:len(ar42)]
surface42 = ax42.contourf(X,Y*delta_t/60,ar42.transpose(),cmap='jet')
ax42.set_title('Set 4:- BC2 solution')
ax42.set_xlabel('Nodes (along the length)')
ax42.set_ylabel('Time (min)')
plt.colorbar(surface42,label='Temperature (K)')
plt.savefig('set_42_plot.png',dpi=400)
#plt.show()

print('plotting done')
print('# # # #')
print('# # # #')
print('# # # #')

#defining function to compare output temperatures to melting temperatures and output where material has failed
print('Now checking whether material has reached glassing/melting temperature')
print('For set 1 solution:-')
print('boundary condition 1')

for i in range(1,len(ar11)-1):
    for j in range(0,ar11.shape[1]-1):
        if ar11[i][j] > 713.15 and mat1[j]=='Felt':
            print('WARNING!!! Felt has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar11[i][j] > 523.15 and mat1[j]=='Carbon_Fibre':
            print('WARNING!!! Carbon Fibre has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar11[i][j] > 493.15 and mat1[j]=='Epoxy':
            print('WARNING!!! Epoxy has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar11[i][j] > 933.7 and mat1[j]=='Aluminium':
            print('WARNING!!! Aluminium has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar11[i][j] > 1505.372 and mat1[j]=='Steel':
            print('WARNING!!! Steel has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
    else:
        continue
    break

print('boundary condition 2 on both sides')

for i in range(1,len(ar12)-1):
    for j in range(0,ar12.shape[1]-1):
        if ar12[i][j] > 713.15 and mat1[j]=='Felt':
            print('WARNING!!! Felt has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar12[i][j] > 523.15 and mat1[j]=='Carbon_Fibre':
            print('WARNING!!! Carbon Fibre has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar12[i][j] > 493.15 and mat1[j]=='Epoxy':
            print('WARNING!!! Epoxy has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar12[i][j] > 933.7 and mat1[j]=='Aluminium':
            print('WARNING!!! Aluminium has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar12[i][j] > 1505.372 and mat1[j]=='Steel':
            print('WARNING!!! Steel has reached glassing Temperature at'+str(i*delta_t)+' seconds')
            break
    else:
        continue
    break


print('For set 2 solution:-')
print('boundary condition 1')


for i in range(1,len(ar21)-1):
    for j in range(0,ar21.shape[1]-1):
        if ar21[i][j] > 713.15 and mat2[j]=='Felt':
            print('WARNING!!! Felt has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar21[i][j] > 523.15 and mat2[j]=='Carbon_Fibre':
            print('WARNING!!! Carbon Fibre has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar21[i][j] > 493.15 and mat2[j]=='Epoxy':
            print('WARNING!!! Epoxy has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar21[i][j] > 933.7 and mat2[j]=='Aluminium':
            print('WARNING!!! Aluminium has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar21[i][j] > 1505.372 and mat2[j]=='Steel':
            print('WARNING!!! Steel has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
    else:
        continue
    break

print('boundary condition 2 on both sides')

for i in range(1,len(ar22)-1):
    for j in range(0,ar22.shape[1]-1):
        if ar22[i][j] > 713.15 and mat2[j]=='Felt':
            print('WARNING!!! Felt has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar22[i][j] > 523.15 and mat2[j]=='Carbon_Fibre':
            print('WARNING!!! Carbon Fibre has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar22[i][j] > 493.15 and mat2[j]=='Epoxy':
            print('WARNING!!! Epoxy has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar22[i][j] > 933.7 and mat2[j]=='Aluminium':
            print('WARNING!!! Aluminium has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar22[i][j] > 1505.372 and mat2[j]=='Steel':
            print('WARNING!!! Steel has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
    else:
        continue
    break


print('For set 3 solution:-')
print('boundary condition 1')


for i in range(1,len(ar31)-1):
    for j in range(0,ar31.shape[1]-1):
        if ar31[i][j] > 713.15 and mat3[j]=='Felt':
            print('WARNING!!! Felt has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar31[i][j] > 523.15 and mat3[j]=='Carbon_Fibre':
            print('WARNING!!! Carbon Fibre has reached glassing Temperature at'+str(i*delta_t)+' seconds')
            break
        elif ar31[i][j] > 493.15 and mat3[j]=='Epoxy':
            print('WARNING!!! Epoxy has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar31[i][j] > 933.7 and mat3[j]=='Aluminium':
            print('WARNING!!! Aluminium has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar31[i][j] > 1505.372 and mat3[j]=='Steel':
            print('WARNING!!! Steel has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
    else:
        continue
    break

print('boundary condition 2 on both sides')

for i in range(1,len(ar32)-1):
    for j in range(0,ar32.shape[1]-1):
        if ar32[i][j] > 713.15 and mat3[j]=='Felt':
            print('WARNING!!! Felt has reached glassing Temperature at at '+str(i*delta_t)+' seconds')
            break
        elif ar32[i][j] > 523.15 and mat3[j]=='Carbon_Fibre':
            print('WARNING!!! Carbon Fibre has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar32[i][j] > 493.15 and mat3[j]=='Epoxy':
            print('WARNING!!! Epoxy has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar32[i][j] > 933.7 and mat3[j]=='Aluminium':
            print('WARNING!!! Aluminium has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar32[i][j] > 1505.372 and mat3[j]=='Steel':
            print('WARNING!!! Steel has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
    else:
        continue
    break


print('For set 4 solution:-')
print('boundary condition 1')


for i in range(1,len(ar41)-1):
    for j in range(0,ar41.shape[1]-1):
        if ar21[i][j] > 713.15 and mat4[j]=='Felt':
            print('WARNING!!! Felt has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar41[i][j] > 523.15 and mat4[j]=='Carbon_Fibre':
            print('WARNING!!! Carbon Fibre has reached glassing Temperature '+str(i*delta_t)+' seconds')
            break
        elif ar41[i][j] > 493.15 and mat4[j]=='Epoxy':
            print('WARNING!!! Epoxy has reached glassing Temperature '+str(i*delta_t)+' seconds')
            break
        elif ar41[i][j] > 933.7 and mat4[j]=='Aluminium':
            print('WARNING!!! Aluminium has reached glassing Temperature '+str(i*delta_t)+' seconds')
            break
        elif ar41[i][j] > 1505.372 and mat4[j]=='Steel':
            print('WARNING!!! Steel has reached glassing Temperature '+str(i*delta_t)+' seconds')
            break
    else:
        continue
    break

print('boundary condition 2 on both sides')

for i in range(1,len(ar42)-1):
    for j in range(0,ar42.shape[1]-1):
        if ar42[i][j] > 713.15 and mat4[j]=='Felt':
            print('WARNING!!! Felt has reached glassing Temperature at '+str(i*delta_t)+' seconds')
            break
        elif ar42[i][j] > 523.15 and mat4[j]=='Carbon_Fibre':
            print('WARNING!!! Carbon Fibre has reached glassing Temperature '+str(i*delta_t)+' seconds')
            break
        elif ar42[i][j] > 493.15 and mat4[j]=='Epoxy':
            print('WARNING!!! Epoxy has reached glassing Temperature '+str(i*delta_t)+' seconds')
            break
        elif ar42[i][j] > 933.7 and mat4[j]=='Aluminium':
            print('WARNING!!! Aluminium has reached glassing Temperature '+str(i*delta_t)+' seconds')
            break
        elif ar42[i][j] > 1505.372 and mat4[j]=='Steel':
            print('WARNING!!! Steel has reached glassing Temperature '+str(i*delta_t)+' seconds')
            break
    else:
        continue
    break

print('checking done')
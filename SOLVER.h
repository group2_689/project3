//all in SI units (MKS)

#ifndef SOLVER_H
#define SOLVER_H

#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include "Mesh1D.h"
#include<armadillo>

class SOLVER
{   
    public :
            double t_end;      //total time of the simuation in s
            double delta_t;     //time step in s
            double Nt;          //total number of time steps
            double q_o1;        //first node flux in W/m^2
            double q_o2;        //last node flux  in W/m^2
            double T_o;         //intitial condition Temp at all nodes (@t=0s) in K
            



    //default constructor
    SOLVER(double t_end, double Nt,double T_o);

    //crank nicolsan solver(implicit and uncondtionally stable)
    double CS_solver(double t_step,double q_o1,double q_o2,std::vector<Node> myNodes,std::string file_name);

    //writing CSV file
    double writeCSV(arma::mat,std::vector<Node>,std::string);
    
};

#endif
!--- Authors: Tanmaye, Suhas, Sidhdharth Sundar, Taimoor Daud Khan, Megan Huebschman
      Updated: 12/04/22
-->

# **1D Heat Equation**

This repository contains the code for solving the 1D Heat Equation to model heat transfer in an object. The code was written as a part of a project for the the MEEN-689 Computing Concepts course taught at Texas A&M University.

## Features

The different features of the 1D Heat Equation solver include:
* A mesh to model the heat equation over time throughout an object.
* The ability to account for different materials with different material properties.
* Flexible number of nodes in mesh to allow accuracy/time tradeoff.
* Check for glassing and melting temperatures of material

### Limitations

* 1D representation only accounts for heat tranfer in one direction of mesh.
* Doesn't account for material properties changing with temperature/glassing.
* Starting temperature is unform

## Compiling and Running
  
The code makes use of the C++ library ```Armadillo``` distributed under the Apache 2.0 license. As such, it requires ```Armadillo``` to be installed. To install ```Armadillo``` visit https://arma.sourceforge.net/ 
Or simply type the following commands in your unix system terminal
If not present already, install LAPACK, Boost and BLAS with 
```
sudo apt-get install liblapack-dev
sudo apt-get install libblas-dev
sudo apt-get install libboost-dev
 ``` 
 Install Armadillo using 
 ```
 sudo apt-get install libarmadillo-dev
 ``` 
 To compile and run the code make sure that your pwd is correct and simply type the following command in your unix system terminal
 ```
 make
 ``` 
 
## Sample Code

The following code snippet shows how to use the Heat Equation solver to model a sphere devided into 8 geometrically symmetric sections starting at an initial temperature of 200 K with varying but radially and compositionally symmetric lay-ups in each octant


```
#include "ReaderCSV.h"

int main()
#include<iostream>
#include<iomanip>
// Include the following header files in addition to the
// ones above
#include "SOLVER.h"
#include "Mesh1D.h"


// The following rules have been used for nomenclature
// myObj        - for objects
// MyClass      - for classes
// My_Function  - for functions

int main()
{  
    // Define solver input parameters
    double tEnd=20*60;      // Total time (seconds);
    double deltaT=4;        // Time step (seconds)
    double nT=tEnd/deltaT;  // Total number of time points
    double T_o=200;          // Initial temperature (kelvin)

    /* Define material objects
    All quantities are specified in SI units
    Arguments: 	    1. Material index
                    2. Material Name
                    3. Material thermal conductivity
                    4. Material density
                    5. Material specific heat capacity
                    6. Material thermal diffusivity
                    7. Material crystallization temperature
    */ 
    Material mat1(1,         "Felt", 1.15, 2230,  830, 6.21e-07,  713.15);
    Material mat2(2, "Carbon_Fibre", 4.00, 1750, 1100, 2.08e-06,  523.15);
    Material mat3(3,        "Epoxy", 0.20, 1100, 1025, 1.77e-07,  493.15);
    Material mat4(4,    "Aluminium",  130, 2810,  933.7, 4.82e-05,  500.00);
    Material mat5(5,        "Steel",   15, 7850,  1505.3, 3.82e-06, 1673.15);

    /* Define layer objects
    Arguments:	1. Layer index
 		        2. Material object associated with this layer
 		        3. Start location
 		        4. Thickness
 		        5. Number of nodes
    */
    // ******** O C T E T  P A I R # 1 ********
    // We will begin by defining the 14 layers of the 1st and 5th octets
    Layer l1(1,   mat1,      0.0,  0.02,  600);
    Layer l2(2,   mat2,  l1.endX, 0.005,  500);
    Layer l3(3,   mat3,  l2.endX, 0.001,  400);
    Layer l4(4,   mat2,  l3.endX, 0.008,  700);
    Layer l5(5,   mat3,  l4.endX, 0.001,  400);
    Layer l6(6,   mat4,  l5.endX, 2.000, 1000);
    Layer l7(7,   mat5,  l6.endX, 1.965, 1000);
    Layer l8(8,   mat5,  l7.endX, 1.965, 1000);
    Layer l9(9,   mat4,  l8.endX, 2.000, 1000);
    Layer l10(10, mat3,  l9.endX, 0.001,  400);
    Layer l11(11, mat2, l10.endX, 0.008,  700);
    Layer l12(12, mat3, l11.endX, 0.001,  400);
    Layer l13(13, mat2, l12.endX, 0.005,  500); 
    Layer l14(14, mat1, l13.endX, 0.020,  600);

    // Next we need to create a set (vector) of the layers we just defined
    std::vector<Layer> layerSet11;
    layerSet11 = {l1, l2,  l3,  l4,  l5,  l6,  l7,
                  l8, l9, l10, l11, l12, l13, l14};
    
    /* We can now create our mesh object
    Arguments: 	    1. Some descriptive identifier for this mesh object
 		            2. Global start node index
    */
    Mesh M1("OctetPair_1_Mesh.txt",0);
    
    /* Using the Mesh_Domain function we will mesh the domain (wow!)
    Arguments:	1. Set (vector) of layers
 		        2. Mesh information Flag (Setting this to true will create a
                                          a mesh information file)
                3. Mesh output filename
    */

    std::vector<Node> myNodes1;
    myNodes1= M1.Mesh_Domain(layerSet11, true, M1.identifier);

    // SOLVER object initialization
    //SOLVER s1(tEnd, nT, to);
    std::cout<<"1st set solution"<<std::endl;

    /* All that is left to be done now is to use the CS_Solver function
       to get the Implicit finite difference solution for the specified
       initial and boundary conditions.
    Arguments:       1. Time step
                     2. Start node heat flux
                     3. End node flux
                     4. Node set
                     5. Job filename
    */

    //declare SOLVER constructor
    SOLVER s1(tEnd,nT,T_o);

    
    // Boundary condition I  (flux only at the start node)
    s1.CS_solver(deltaT, 20000,     0, myNodes1,"set_11.csv");

    // Boundary condition II (flux at start and end node)
    s1.CS_solver(deltaT, 20000, 20000, myNodes1,"set_12.csv");


   // ***********************************************************
   // All the other three pairs can be solved in an exactly
   // similar fashion. We have presented the setup below in 
   // a stepwise manner.
   // ***********************************************************

   // STEP 1 ----- Creating Layer objects -----
   // OCTET PAIR 2 - Octets 2 & 6
    Layer l15(1,  mat1,       0,  0.03,   600);
    Layer l16(2,  mat2, l15.endX, 0.003,  500);
    Layer l17(3,  mat3, l16.endX, 0.001,  400);
    Layer l18(4,  mat2, l17.endX, 0.012,  700);
    Layer l19(5,  mat3, l18.endX, 0.001,  400);
    Layer l20(6,  mat4, l19.endX, 2.300, 1000);
    Layer l21(7,  mat5, l20.endX, 1.653, 1000);
    Layer l22(8,  mat5, l21.endX, 1.653, 1000);
    Layer l23(9,  mat4, l22.endX, 2.300, 1000);
    Layer l24(10, mat3, l23.endX, 0.001,  400);
    Layer l25(11, mat2, l24.endX, 0.012,  700);
    Layer l26(12, mat3, l25.endX, 0.001,  400);
    Layer l27(13, mat2, l26.endX, 0.003,  500);
    Layer l28(14, mat1, l27.endX, 0.030,  600);

    // OCTET PAIR 3 - Octets 3 & 7
    Layer l29(1,  mat1,        0, 0.020,  600);
    Layer l30(2,  mat2, l29.endX, 0.010,  500);
    Layer l31(3,  mat3, l30.endX, 0.001,  400);
    Layer l32(4,  mat2, l31.endX, 0.012,  700);
    Layer l33(5,  mat3, l32.endX, 0.001,  400);
    Layer l34(6,  mat4, l33.endX, 1.700, 1000);
    Layer l35(7,  mat5, l34.endX, 2.256, 1000);
    Layer l36(8,  mat5, l35.endX, 2.256, 1000);
    Layer l37(9,  mat4, l36.endX, 1.700, 1000);
    Layer l38(10, mat3, l37.endX, 0.001,  400);
    Layer l39(11, mat2, l38.endX, 0.012,  700);
    Layer l40(12, mat3, l39.endX, 0.001,  400);
    Layer l41(13, mat2, l40.endX, 0.010,  500);
    Layer l42(14, mat1, l41.endX, 0.020,  600);

    // OCTET PAIR 4 - Octets 4 & 8
    Layer l43(1,  mat1,        0, 0.0050,  300);
    Layer l44(2,  mat2, l43.endX, 0.0001,  300);
    Layer l45(3,  mat3, l44.endX, 0.0010,  400);
    Layer l46(4,  mat2, l45.endX, 0.0001,  300);
    Layer l47(5,  mat3, l46.endX, 0.0010,  400);
    Layer l48(6,  mat4, l47.endX, 2.8000, 1000);
    Layer l49(7,  mat5, l48.endX, 1.1928, 1000);
    Layer l50(8,  mat5, l49.endX, 1.1928, 1000);
    Layer l51(9,  mat4, l50.endX, 2.8000, 1000);
    Layer l52(10, mat3, l51.endX, 0.0010,  400);
    Layer l53(11, mat2, l52.endX, 0.0001,  300);
    Layer l54(12, mat3, l53.endX, 0.0010,  400);
    Layer l55(13, mat2, l54.endX, 0.0001,  300);
    Layer l56(14, mat1, l55.endX, 0.0050,  300);

    // STEP 2 ----- Creating Layer object sets -----
    std::vector<Layer> layerSet_2={l15,l16,l17,l18,l19,l20,l21,l22,l23,l24,l25,l26,l27,l28};
    std::vector<Layer> layerSet_3={l29,l30,l31,l32,l33,l34,l35,l36,l37,l38,l39,l40,l41,l42};
    std::vector<Layer> layerSet_4={l43,l44,l45,l46,l47,l48,l49,l50,l51,l52,l53,l54,l55,l56};

    // STEP 3 ----- Creating Mesh objects -----
    Mesh M2("OctetPair_2_Mesh.txt",0);
    Mesh M3("OctetPair_3_Mesh.txt",0);
    Mesh M4("OctetPair_4_Mesh.txt",0);

    // STEP 4 ----- Meshing the multilayered domain -----
    std::vector<Node> myNodes2= M2.Mesh_Domain(layerSet_2, true, M2.identifier);
    std::vector<Node> myNodes3= M3.Mesh_Domain(layerSet_3, true, M3.identifier);
    std::vector<Node> myNodes4= M4.Mesh_Domain(layerSet_4, true, M4.identifier);

    // STEP 5 ----- Setting up the solver and solving -----
    std::cout<<"2nd set solution"<<std::endl;
    s1.CS_solver(deltaT, 40000,     0, myNodes2, "set_21.csv");
    s1.CS_solver(deltaT, 40000, 40000, myNodes2, "set_22.csv");

    std::cout<<"3rd set solution"<<std::endl;
    s1.CS_solver(deltaT, 32000,     0, myNodes3, "set_31.csv");
    s1.CS_solver(deltaT, 32000, 32000, myNodes3, "set_32.csv");

    std::cout<<"4th set solution"<<std::endl;
    s1.CS_solver(deltaT, 50000,     0, myNodes4, "set_41.csv");
    s1.CS_solver(deltaT, 50000, 50000, myNodes4, "set_42.csv");
    
    return 0;
}

```

## Usage

The solver class has the following attributes

* **t_end**: total time of the simuation in s.
* **delta_t**: time step in s.
* **Nt**: total number of time steps.
* **q_o1**: first node flux in W/m^2.
* **q_o2**: last node flux  in W/m^2.
* **T_o**: intitial condition Temp at all nodes (@t=0s) in K.

The different methods implemented in the solver class are as follows

### Constructor: Solver(double t_end, double Nt, double T_o)
The constructor takes in 3 arguments and sets them to t_end, Nt, and T_o attributes of the class.

### CS_solver(double t_step,double q_o1,double q_o2,std::vector<Node> myNodes,std::string file_name)
The cs_solver function uses crank nicolsan solver (implicit and uncondtionally stable).

### writeCSV(arma::mat,std::vector<Node>,std::string)
The writeCSV function outputs a CSV file with the data.

The Material class has the following attributes
* **index**: Material index.
* **name**: Material name.
* **k**: Material thermal conductivity.
* **rho**: Material density.
* **cP**: Material specific heat capacity.
* **alpha**: Material thermal diffusivity.
* **tCryst**: Material crystallization temperature.

The different methods implemented in the Material class are as follows

### Constructor: Material(int para1, std::string para2, float para3, float para4, float para5, float para6, float para7)
The constructor takes in 7 arguments and sets them to index, name, k, rho, cP, alpha, and tCryst attributes of the class.

The Layer class has the following attributes
* **index**: Layer index.
* **material**: Layer material.
* **startX**: Layer start coordinate.
* **endX**: Layer end coordinate.
* **thickness**: Layer thickness.
* **nodeCount**: Layer node count.

The different methods implemented in the Layer class are as follows

### Constructor: Layer(int para1, Material para2, float para3, float para5, int para6)

The constructor takes in 5 arguments and sets them to index, material startX, thickness, nodeCount attributes of the class and calculates endX.

The node class has the following attributes
* **index**: Node index.
* **xLoc**: Node x-coordinate.
* **xStep**: X step size.
In addition, node objects contain all of the attributes of the Material class

The MeshLayer class has the following attributes
* **layer**: Member Initializer List for passing object as constructor parameter.
* **startNode**: Start node label of the layer.
* **endNode**: End node label.
* **nodeCount**: Count of nodes.

The different methods implemented in the MeshLayer class are as follows

### Constructor: MeshLayer(Layer para1, int para2)
The constructor takes in 2 arguments and sets them to layer and startNode and calculates endNode.

### Mesh_Layer()
The Mesh_Layer function generate nodes inside the associated layer. Layer to be meshed is already assigned in the constructor.

### Display_Details(std::vector<Node> nodeSet)
The Display_Details function displays information of the generared nodes.

### Mesh_Layer_ConstX(float xStep)
The Mesh_Layer_ConstX function generates nodes inside the associated layer with given step.

The Mesh class has the following attributes
* **identifier**: String identifier e.g. "Mesh for octet 1".
* **startNode**: Global start node index.

The different methods implemented in the Mesh class are as follows

### Constructor: Mesh(std::string para1, int para2)
The constructor takes in 2 arguments and sets them to identifier and startNode attributes.

### Mesh_Domain(std::vector<Layer> layerSet, bool meshInfoFlag, std::string meshFilename)
The Mesh_Domain function creates a set of all nodes in the domain

### Postprocessing
To get time vs nodes Temperature contours and melting temperature comparisons please run plotting.py.
Just mention the time step size in the program.

// AUTHOR: TEAM ROCKET
// CONTRIBUTORS: MEGAN H & TANMAYE Y. HEBLEKAR
// DATE & TIME: 11/24/2022 | 7.07 PM
// PROJECT 3 MEEN-689

#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <vector>
#include <iomanip>

// The following rules have been used for nomenclature
// myObj        - for objects
// MyClass      - for classes
// My_Function  - for functions

class Material
{	
	public:
	// Material class attributes
	// See constructor for attribute details
	int index;
	std::string name;
	float k;
	float rho;
	float cP;
	float alpha;
	float tCryst;
	// Parameterized constructor for Material class
	Material(int para1, std::string para2, float para3, 
		 	 float para4, float para5, float para6, float para7);
	
};

class Layer
{
	public:
	// Layer class attributes
	// See constructor for attribute details
	int index;
	Material material;
	float startX;
	float endX;
	float thickness;
	int nodeCount;
	
	// Parameterized constructor for Layer class
	Layer(int para1, Material para2, float para3, 
	      float para5, int para6);
	
};

class Node
{
	public:
	// Node class attributes
	// Note: solver directly interacts with instances of this class
	int index;   // Node index
	float xLoc;  // Node x-coordinate
	float xStep; // X step size
	// Following are material properties associated with a node
	std::string matName;
	float k;
	float rho;
	float cP;
	float alpha;
	float tCryst;
};

class MeshLayer 
{
	// This class is meant to create a set of nodes for a single layer
	public:
	Layer layer;
	int startNode;
	int endNode;
	int nodeCount = layer.nodeCount;
	
	// Parameterized constructor for MeshLayer class
	MeshLayer(Layer para1, int para2);	    
	
	// Function to generate nodes inside the associated layer
	// Layer to be meshed is already assigned in the constructor
	std::vector<Node> Mesh_Layer();
	
	// Function to display information of the generared nodes
	void Display_Details(std::vector<Node> nodeSet);

	// Function to generate nodes inside the associated layer with given step
	std::vector<Node> Mesh_Layer_ConstX(float xStep);
	
};

class Mesh
{
	public:
	// Mesh class attributes
	// See constructor for attribute details
	std::string identifier;
	int startNode;
	
	// Parameterized constructor for Mesh class
	Mesh(std::string para1, int para2);
	
	// Function to create set of all nodes in the domain
	std::vector<Node> Mesh_Domain(std::vector<Layer> layerSet, 
								  bool meshInfoFlag, std::string meshFilename);
								  
};

#endif
